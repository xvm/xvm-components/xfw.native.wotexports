# Changelog

## v1.26.0

* add support for WoT 1.26.0
* drop 32-bit support

## v1.18.0

* first version
